package com.grupoica.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HolaServlet
 */
@WebServlet("/hola")
public class HolaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HolaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String strNombre = request.getParameter("nombre");
		String html = "<html><head><title>Formulario envio</title></head>" + 
				"<body>";
		if (strNombre == null || "".equals(strNombre)) {
			html += "<h2>Pon el nombre</h2>";
		} else {
			html += "<form action='./hola.do' method='post'>" + 
			"	Veces: <input name='veces' type='number'/>" + 
			"	<input type='submit' value='Enviar por POST'/>" + 
			"</form>";			
		}
		html+="</body></html>";
		response.getWriter().append(html);
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try (PrintWriter escritor = response.getWriter()) {
			String strVeces = request.getParameter("veces");
			escritor.println("<html><head><title>Datos</title></head><body>");
			if (strVeces == null || "".equals(strVeces)) {
				System.out.println("Usuario no ha puesto datos");
				escritor.println("<h2>Falta n� veces</h2>");
			} else {
				System.out.println("Usuario ha puesto " + strVeces + " veces");
				int v = Integer.parseInt(strVeces);
				escritor.println("<ul>");
				for (int i = 0; i < v; i++) {
					escritor.println("<li> " + i + " vez </li>");
				}
				escritor.println("</ul>");
			}
			escritor.println("</body></html>");
		}
		
		response.getWriter().append("<h1>Hola desde servlet</h1> at: ").append(request.getContextPath());
	}
}
