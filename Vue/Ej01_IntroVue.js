Vue.component("app-saludo", {
    "template": `<h2>Hola soy un componente</h2>`
});
Vue.component("app-despedida", {
    "template": `<div><h2>A la voluntad de Dios</h2>
    <h2>Pues eso hasta luego!</h2>
    </div>`
});
new Vue({
    "el": "#app-section",
    template: `<div><h2>Hola ICA</h2>
    <app-saludo></app-saludo>
    <app-despedida></app-despedida>
    </div>`
});
new Vue({
    el: "#app-section-2"
});