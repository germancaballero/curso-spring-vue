package com.grupoica.appspring.apirest.controladores;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
//Con esto le decimos que es un servlet pero gestionado por Spring

import com.grupoica.appspring.apirest.modelo.Aumento;
import com.grupoica.appspring.apirest.modelo.AumentosRepoDAO;

@RestController	
@RequestMapping("/api/aumentos")
public class ControladorAumentos {

	/* Vamos a decirle a Spring que con nos instancie un objeto que
	 * implemente la interfaz AumentosRepoDAO, y que lo asigne a nuestra
	 * propiedad daoAum. No vamos a hacer new(), lo hará Spring.
	 * Inyección de depencias. 
	*/
	@Autowired
	AumentosRepoDAO daoAum;
	 /*
	@RequestMapping(method=RequestMethod.GET, value="/prueba")
	public Aumento pruebaAumento() {
		return new Aumento(1, "Marcos", 5.10f);
	}*/
	@PostMapping
	public Aumento crear(@RequestBody Aumento nuevoAumento ) {
		// 
		return daoAum.saveAndFlush(nuevoAumento);
	}
	@GetMapping
	public List<Aumento> leerTodos() {
		return daoAum.findAll();
	}
	@RequestMapping(value="{id}", method=RequestMethod.GET)
	public Aumento leertUno(@PathVariable int id) {
		if (id > 0) {
			Optional<Aumento> a = daoAum.findById(id);
			if (a.isPresent()) 
				return a.get();
			 else 
				return null;
		}
		else 
			return null;
	}
	/* Metodos PUT y DELETE para el resto de CRUD */
	/* Entidad empleado con su repo y sus Controlador rest: id y Nombre */
	@RequestMapping(value ="{id}", method=RequestMethod.DELETE)
	public boolean borrarUno(@PathVariable int id) {
		if (id > 0) {
			daoAum.deleteById(id);
				return true;
		}
		else
			return false;
	}
	@PutMapping
    public Aumento modificarUno(@RequestBody Aumento nuevoAumento) {
        return daoAum.saveAndFlush(nuevoAumento);
    }

}
