package com.grupoica.appspring.apirest.modelo;

import java.util.List;
import java.util.Set;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
public class Empleado {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	int id;
	
	@Basic(optional = false)	//NOT NULL
	String nombre;
	
	@OneToMany(mappedBy="empleado", fetch=FetchType.EAGER)	
    @JsonManagedReference(value="aumento")
	List<Aumento> aumentos;
		
	public Empleado() {
		super();
	}
	
	public Empleado(int id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}

	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public List<Aumento> getAumentos() {
		return aumentos;
	}

	public void setAumentos(List<Aumento> aumentos) {
		this.aumentos = aumentos;
	}
	
	
	
}
