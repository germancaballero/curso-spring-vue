package com.grupoica.appspring.apirest.controladores;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
//Con esto le decimos que es un servlet pero gestionado por Spring

import com.grupoica.appspring.apirest.modelo.Empleado;
import com.grupoica.appspring.apirest.modelo.EmpleadoRepoDAO;

@RestController	
@RequestMapping("/api/empleados")
public class ControladorEmpleados {

	/* Vamos a decirle a Spring que con nos instancie un objeto que
	 * implemente la interfaz EmpleadosRepoDAO, y que lo asigne a nuestra
	 * propiedad daoAum. No vamos a hacer new(), lo hará Spring.
	 * Inyección de depencias. 
	*/
	@Autowired
	EmpleadoRepoDAO daoEmp;
	 
	@PostMapping
	public Empleado crear(@RequestBody Empleado nuevoEmpleado ) {
		// 
		return daoEmp.saveAndFlush(nuevoEmpleado);
	}
	@GetMapping
	public List<Empleado> leerTodos() {
		return daoEmp.findAll();
	}
	@RequestMapping(value="{id}", method=RequestMethod.GET)
	public Empleado leertUno(@PathVariable int id) {
		if (id > 0) {
			Optional<Empleado> a = daoEmp.findById(id);
			if (a.isPresent()) 
				return a.get();
			 else 
				return new Empleado(-1, "Error");
		}
		else 
			return null;
	}
	/* Metodos PUT y DELETE para el resto de CRUD */
	/* Entidad empleado con su repo y sus Controlador rest: id y Nombre */
	@RequestMapping(value ="{id}", method=RequestMethod.DELETE)
	public boolean borrarUno(@PathVariable int id) {
		if (id > 0) {
			daoEmp.deleteById(id);
				return true;
		}
		else
			return false;
	}
	@PutMapping
    public Empleado modificarUno(@RequestBody Empleado nuevoEmpleado) {
        return daoEmp.saveAndFlush(nuevoEmpleado);
    }
	@RequestMapping(value="{id}",method=RequestMethod.PUT)
	public Empleado modificarEmpleado(@PathVariable int id,
			@RequestBody Empleado empleado) {
		//El request body indica que el parámetro que llega 
		//corresponde al cuerpo de la petición
		
		 if(id>0) {
			 Optional<Empleado> e = daoEmp.findById(id);//Busca el id
			 if(e.isPresent()) {//Si el optional encuentra el id devuelve lo encontrado
				 
				 empleado.setId(id);
				 empleado = daoEmp.saveAndFlush( empleado);
				 return  empleado;
			 }else {//Si el optional no devuelve nada devolvemos error
				 return new Empleado(-1,"Error");
			 }
		 }else
			return null;
		
	}

}
