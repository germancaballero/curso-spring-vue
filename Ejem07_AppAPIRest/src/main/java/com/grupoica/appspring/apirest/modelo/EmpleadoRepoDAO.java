package com.grupoica.appspring.apirest.modelo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface EmpleadoRepoDAO extends JpaRepository<Empleado, Integer> {
	
}
