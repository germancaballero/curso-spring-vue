package com.grupoica.appspring.apirest.modelo;

import javax.persistence.Basic;
// Esto es JPA: javax.persistence
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.springframework.lang.NonNull;

import com.fasterxml.jackson.annotation.JsonBackReference;


// Convertir el POJO en una Entidad que se persista en bb.dd.
// Una entidad es al back lo que el componente es a web: Clase, Tabla en bbdd, 
@Entity
public class Aumento {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)	// AUTOINCREMENT
	private int id;
	
	@Basic(optional = false)	// NOT NULL
	// @NonNull	
	// @NotNull
	@ManyToOne
    @JoinColumn(name = "empleado", referencedColumnName = "id")
	@JsonBackReference(value="empleado")
	private Empleado empleado;
	
	// @Min
	float dinero;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Empleado getEmpleado() {
		return empleado;
	}
	public void setEmpleado(Empleado empleado) {
		this.empleado = empleado;
	}
	public float getDinero() {
		return dinero;
	}
	public void setDinero(float dinero) {
		this.dinero = dinero;
	}	
	
}
