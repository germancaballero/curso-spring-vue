package com.grupoica.appspring.modelo.daos;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;

import com.grupoica.appspring.modelo.entidades.Heroe;

public interface HeroesDAO extends JpaRepository<Heroe, Integer> {

}
